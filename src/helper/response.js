const response = (payload, ...rest) => {
    return {
      status: "thành công",
      data: payload,
      ...rest,
    };
  };
  
  module.exports = {
    response,
  };
  